<%-- 
    Document   : edit_profil
    Created on : Dec 18, 2014, 4:54:17 PM
    Author     : yannissolemale
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit profile page</title>
        <link href="<c:url value="/jsp/bootstrap/css/bootstrap.css" />" rel="stylesheet">
    </head>
    <body>
        
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          <li><a href="<c:url value='/profil' />">Profile</a></li>
          <li><a href="<c:url value='/contact' />">Contacts</a></li>
          <li><a href="<c:url value='/sendmessage' />">Messages</a></li>
          <li><a href="<c:url value='/offer' />">View offer</a></li>
          <li><a href="<c:url value='/logout' />">Logout</a></li>
        </ul>
    </div>
        
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span6 offset5">

            <h1>Edit my profile</h1>

            <form method="POST">
                <div>
                    <label for="username">Firstname:</label>
                    <input type="text" name="firstname" />
                </div>
                <div>
                    <label for="username">Lastname:</label>
                    <input type="text" name="lastname" />
                </div>
                <div>
                    <label for="password">Password:</label>
                    <input type="password" name="password" />
                </div>
                <div>
                    <label for="username">Address:</label>
                    <input type="text" name="adress" />
                </div>
                <div>
                    <label for="username">Email Adress:</label>
                    <input type="text" name="mail" />
                </div>
                <div>
                    <label for="username">Card Number:</label>
                    <input type="text" name="card" />
                </div>
                <div>
                    <input class="btn btn-info" type="submit" value="Submit" />
                </div>
            </form>

                <a href="<c:url value="/profil" />">Back to Profil</a>
        </div>
      </div>
    </div>
    
    </body>
</html>
