/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.web.servlet;

import com.supinfo.supsms.entity.User;
import com.supinfo.supsms.service.AdminService;
import java.util.List;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pascal
 */

@WebServlet(name = "AdminServlet", urlPatterns = {"/admin"})
public class AdminServlet extends HttpServlet {
    
    @EJB
    private AdminService adminService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idParam = req.getParameter("id");
        Long userId = Long.valueOf(idParam);
        
        adminService.deleteUser(userId);
        User user = (User) req.getSession().getAttribute("user");
        req.getSession().setAttribute("user", adminService.findUserById(user.getId()));

        resp.sendRedirect(getServletContext().getContextPath() + "/admin");
    }
    
}
