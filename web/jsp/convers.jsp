<%-- 
    Document   : convers
    Created on : 13 déc. 2014, 23:31:52
    Author     : Pascal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Conversation</title>
        <link href="<c:url value='/jsp/bootstrap/css/bootstrap.css' />" rel="stylesheet">
    </head>
    <body>
            <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          <li class="active"><a href="<c:url value='/register' />">Register</a></li>
          <li><a href="<c:url value='/login' />">Login</a></li>
          <li><a href="<c:url value='/offer' />">View offer</a></li>
        </ul>
    </div>
        
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="span6 offset5">
            
              <h1>
                  <c:choose>
                        <c:when test="${not empty contactInfo}">
                            <c:out value="${contactInfo.firstName} ${contactInfo.lastName}"/>
                        </c:when>
                        <c:otherwise>
                            <c:out value="${param.nb}"/>
                        </c:otherwise>   
                  </c:choose>
              </h1>
                  <c:if test="${not empty messages}">
                    <ul>
                        <c:forEach items="messages" var="message">
                        <li><c:choose>
                                <c:when test="${not empty contactInfo && param.nb == contactInfo.phoneNumber }">
                                    <c:out value="${contactInfo.firstName} : "/>
                                </c:when>
                                <c:when test="${param.nb == user.phoneNumber}">
                                    <c:out value="You : "/>
                                </c:when>
                                <c:otherwise>
                                    <c:out value="${param.nb} : "/>
                                </c:otherwise> 
                          </c:choose>
                          
                          <c:out value="${message.content}" /></li>
                        </c:forEach>
                    </ul>
                  </c:if>
              <form method="POST">
                  
                    <div>
                        <textArea name="content" ></textarea>
                    </div>
                    <div>
                        <input class="btn btn-info" type="submit" value="Submit" />
                    </div>
              </form>
        
        
    </body>
</html>
