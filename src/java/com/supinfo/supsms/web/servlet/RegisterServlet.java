/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.web.servlet;

import com.supinfo.supsms.entity.User;
import com.supinfo.supsms.service.MessageService;
import com.supinfo.supsms.service.UserService;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Opium
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {

    @EJB
    private UserService userService;

    @EJB
    MessageService messageService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/register.jsp").forward(req, resp);
    }

     @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = new User();
        
        user.setFirstName(req.getParameter("firstname"));
        user.setLastName(req.getParameter("lastname"));
        user.setPassword(req.getParameter("password"));
        user.setAdress(req.getParameter("adress"));
        user.setMailAdress(req.getParameter("mail"));
        user.setPhoneNumber(req.getParameter("phone"));
        user.setCardNumber(req.getParameter("card"));
        
        User newuser = userService.addUser(user);
        
        req.getSession().setAttribute("user", user);
        req.getSession().setAttribute("iduser", newuser.getId());
        List<String> convers = messageService.getAllConvers(user);
        req.getSession().setAttribute("convers", convers);
        
        resp.sendRedirect(getServletContext().getContextPath());
        //resp.sendRedirect(req.getContextPath() + "/jsp/register");
    }
}
