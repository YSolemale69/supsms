<%-- 
    Document   : customerRegister
    Created on : 2 déc. 2014, 21:36:41
    Author     : Opium
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
        <link href="<c:url value="/jsp/bootstrap/css/bootstrap.css" />" rel="stylesheet">
    </head>
    <body>
        
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          <li><a href="<c:url value='/login' />">Login</a></li>
          <li class="active"><a href="<c:url value='/register' />">Register</a></li>
        </ul>
    </div>
        
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="span6 offset5">
            
              <h1>Register</h1>
              
                <form method="POST">
                    <div>
                        <label for="username">Firstname:</label>
                        <input type="text" name="firstname" />
                    </div>
                    <div>
                        <label for="username">Lastname:</label>
                        <input type="text" name="lastname" />
                    </div>
                    <div>
                        <label for="password">Password:</label>
                        <input type="password" name="password" />
                    </div>
                    <div>
                        <label for="username">Address:</label>
                        <input type="text" name="adress" />
                    </div>
                    <div>
                        <label for="username">Email Address:</label>
                        <input type="text" name="mail" />
                    </div>
                    <div>
                        <label for="username">Phone:</label>
                        <input type="text" name="phone" />
                    </div>
                    <div>
                        <label for="username">Card Number:</label>
                        <input type="text" name="card" />
                    </div>
                    <div>
                        <input class="btn btn-info" type="submit" value="Submit" />
                    </div>
                </form>
              
          </div>
        </div>
    </div>
    </body>
</html>
