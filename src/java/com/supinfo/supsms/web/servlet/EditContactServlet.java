/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.web.servlet;

import com.supinfo.supsms.entity.Contact;
import com.supinfo.supsms.entity.User;
import com.supinfo.supsms.service.ContactService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pascal
 */
@WebServlet(name = "EditContactServlet", urlPatterns = {"/editcontact"})
public class EditContactServlet extends HttpServlet {
    
    @EJB
    ContactService contactService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String idParam = req.getParameter("id");
        Long contactId = Long.valueOf(idParam);
        req.getSession().setAttribute("contactid", contactId);
        Contact contact = contactService.findContactById(contactId);
        req.setAttribute("contactinfo", contact);
        req.getRequestDispatcher("/jsp/editcontact.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Contact contact = contactService.findContactById((Long) req.getSession().getAttribute("contactid"));
        contact.setPhoneNumber(req.getParameter("phonenumber"));
        contact.setFirstName(req.getParameter("firstname"));
        contact.setLastName(req.getParameter("lastname"));
        contact.setAdress(req.getParameter("adress"));
        contact.setMailAdress(req.getParameter("mail"));
        contact.setUpdated(new java.util.Date().getTime());
        contactService.updateContact(contact);
        req.getSession().removeAttribute("contactid");
        
        resp.sendRedirect(getServletContext().getContextPath() + "/contact");
    }
}
