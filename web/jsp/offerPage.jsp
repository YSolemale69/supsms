<%-- 
    Document   : offerPage
    Created on : 13 déc. 2014, 22:56:54
    Author     : Pascal
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Offer page</title>
        <link href="<c:url value="/jsp/bootstrap/css/bootstrap.css" />" rel="stylesheet">
    </head>
    <body>
        
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          <li><a href="<c:url value='/profil' />">Profile</a></li>
          <li><a href="<c:url value='/contact' />">Contacts</a></li>
          <li><a href="<c:url value='/sendmessage' />">Messages</a></li>
          <li class="active"><a href="<c:url value='/offer' />">View offer</a></li>
          <li><a href="<c:url value='/logout' />">Logout</a></li>
        </ul>
    </div>
        
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span10 offset1">
                <h1>Premium user offer</h1>
                
                <br>
                
                <h3>10$/month </h3>
                <h3>unlimited contact</h3>
                <h3>unlimited send</h3>
                <h3>all messages synchronized</h3>
                
                <div>
                    <input class="btn btn-info"  value="Subscribe" />
                </div>
                
            </div>
        </div>
    </div>
    </body>
</html>
