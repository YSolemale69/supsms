/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.dao;

import com.supinfo.supsms.entity.User;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Opium
 */
@Local
public interface UserDao {
    
    User addUser(User user);
    
    User updateUser(User user);
    
    List<User> getAllUser();
    
    public User findUserByNumberAndPassword(String phoneNumber, String Password);
    
    public User findUserByNumber(String phoneNumber);
            
    public User findUserById(Long UserId);
}
