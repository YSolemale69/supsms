/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.web.servlet;

import com.supinfo.supsms.entity.Contact;
import com.supinfo.supsms.entity.User;
import com.supinfo.supsms.service.ContactService;
import com.supinfo.supsms.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pascal
 */
@WebServlet(name = "ContactServlet", urlPatterns = {"/contact"})
public class ContactServlet extends HttpServlet {

    @EJB
    ContactService contactService;
    
    @EJB
    UserService userService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        User user = userService.findUserById((Long) req.getSession().getAttribute("iduser"));
        List<Contact> contacts = contactService.findContactByUser(user.getId());
        req.setAttribute("contacts", contacts);
        req.getRequestDispatcher("/jsp/contact.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        Contact contact = new Contact();
        contact.setPhoneNumber(req.getParameter("phonenumber"));
        contact.setFirstName(req.getParameter("firstname"));
        contact.setLastName(req.getParameter("lastname"));
        contact.setAdress(req.getParameter("adress"));
        contact.setMailAdress(req.getParameter("mail"));
        User user = (User) req.getSession().getAttribute("user");
        contact.setCreator(user);
        contact.setUpdated(new java.util.Date().getTime());
        contactService.addContact(contact);
        
        req.getSession().setAttribute("user", userService.findUserById(user.getId()));
        
        doGet(req, resp);
    }

}
