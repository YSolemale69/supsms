/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.service;

import com.supinfo.supsms.dao.MessageDao;
import com.supinfo.supsms.entity.Message;
import com.supinfo.supsms.entity.User;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Pascal
 */
@Stateless
public class MessageService {
    
    @EJB
    MessageDao messageDao;
            
    public Message addMessage(Message message){
        return messageDao.addMessage(message);
    }
    
    public List<Message> findMessageBySenderAndReceiver(String sender, String Receiver){
        return messageDao.findMessageBySenderAndReceiver(sender, Receiver);
    }
    
    public List<String> getAllConvers(User user){
        return messageDao.getAllConvers(user);
    }
    
    public List<Message> getAllMessages() {
        return messageDao.getAllMessages();
    }
}
