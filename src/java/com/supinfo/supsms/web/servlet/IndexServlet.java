/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.web.servlet;

import com.supinfo.supsms.service.UserService;
import com.supinfo.supsms.service.MessageService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pascal
 */
@WebServlet(name = "IndexServlet", urlPatterns = {"/"})
public class IndexServlet extends HttpServlet {

    
    @EJB
    UserService userService;
    
    @EJB
    MessageService messageService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        int nbuser = userService.getAllUser().size();
        int nbmsg = messageService.getAllMessages().size();
        req.setAttribute("nbuser", nbuser);
        req.setAttribute("nbmsg", nbmsg);
        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
