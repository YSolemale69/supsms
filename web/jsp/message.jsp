<%-- 
    Document   : message
    Created on : 19 déc. 2014, 00:54:24
    Author     : Rémy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Envoie de Messages</title>
        <link href="<c:url value='/jsp/bootstrap/css/bootstrap.css' />" rel="stylesheet">
    </head>
    <body>
            <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          <li><a href="<c:url value='/profil' />">Profile</a></li>
          <li><a href="<c:url value='/contact' />">Contacts</a></li>
          <li class="active"><a href="<c:url value='/sendmessage' />">Messages</a></li>
          <li><a href="<c:url value='/offer' />">View offer</a></li>
          <li><a href="<c:url value='/logout' />">Logout</a></li>
        </ul>
    </div>
        
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="span6 offset5">
            
              <h1>Message</h1>
              <form method="POST">
                  
                  <div>
                        <label for="content">message:</label>
                        <textArea id="content" name="content" ></textarea>
                        <label for="msg">Destinataire:</label>
                        <SELECT name="receiver" size="1">
                        <c:forEach items="${contacts}" var="contact">
                            <option value="${contact.phoneNumber}">${contact.firstName} ${contact.lastName}</option>                         
                        </c:forEach>
                        </SELECT>
                        <input type="hidden" value ="${sender}" name="sender">
                    </div>
                    <div>
                        <input class="btn btn-info" type="submit" value="Submit" />
                    </div>
              </form>
          </div>
        </div>
    </div>
    </body>
</html>
