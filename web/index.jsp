<%-- 
    Document   : index
    Created on : 2 déc. 2014, 19:16:27
    Author     : Opium
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SupSms</title>
        <link href="<c:url value="/jsp/bootstrap/css/bootstrap.css" />" rel="stylesheet">
    </head>
    <body>
        
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          
          <c:choose>
                    <c:when test="${not empty user}">
                        <li><a href="<c:url value='/profil' />">Profile</a></li>
                        <li><a href="<c:url value='/contact' />">Contacts</a></li>
                        <li><a href="<c:url value='/sendmessage' />">Messages</a></li>
                        <li><a href="<c:url value='/offer' />">View offer</a></li>
                        <li><a href="<c:url value='/logout' />">Logout</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="<c:url value='/login' />">Login</a></li>
                        <li><a href="<c:url value='/register' />">Register</a></li>
                    </c:otherwise>
                </c:choose>
        </ul>
    </div>
        
        <div class="container-fluid">
        <div class="row-fluid">
            <div class="span10 offset1">
                <h1>Some stats</h1>        
                <div>
                    <c:out value="Number of users : ${nbuser}"/>
                </div>
                <div>
                    <c:out value="Number of messages send : ${nbmsg}"/>
                </div>
                <div>
                    <c:if test="${not empty convers}">
                    <ul>
                        <c:forEach items="convers" var="nb">
                            <li><c:url value="/convers" var="conversUrl">
                                    <c:param name="nb" value="${nb}" />
                                </c:url>
                                <a href="${conversUrl}"><c:out value="Conversation with : ${nb}"/></a>
                            </li>
                        </c:forEach>
                    </ul>
                    </c:if>
                </div>
            </div>
        </div>
        </div>
        </body>
</html>

        
