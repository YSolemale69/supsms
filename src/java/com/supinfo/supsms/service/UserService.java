/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.service;

import com.supinfo.supsms.dao.UserDao;
import com.supinfo.supsms.entity.User;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Opium
 */

@Stateless
public class UserService {

    @EJB
    private UserDao userDao;
     
    public User addUser(User user) {
       return userDao.addUser(user);
    }
    
    public User updateUser(User user) {
       return userDao.updateUser(user);
    }

    public List<User> getAllUser() {
        return userDao.getAllUser();
    }

    
    public User findUserByNumberAndPassword(String phoneNumber, String Password){
        return userDao.findUserByNumberAndPassword(phoneNumber, Password);
    }
    
    public User findUserById(Long UserId){
        return userDao.findUserById(UserId);
    }
}
