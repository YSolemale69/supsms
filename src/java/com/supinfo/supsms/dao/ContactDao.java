/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.dao;

import com.supinfo.supsms.entity.Contact;
import java.util.List;

/**
 *
 * @author Pascal
 */
public interface ContactDao {
    
    public Contact addContact(Contact contact);
    
    public Contact updateContact(Contact contact);
    
    public Contact removeContact(Long contactId);
    
    public Contact findContactById(Long contactId);
    
    public List<Contact> findContactByUser(Long userId);
    
    public Contact findContactByUserAndNumber(Long userId, String phoneNumber);

    public Contact findContactByFirstNameAndLastName(String Firstname, String Lastname);
 
}
