<%-- 
    Document   : profil
    Created on : Dec 16, 2014, 12:40:41 PM
    Author     : yannissolemale
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profile page</title>
        <link href="<c:url value="/jsp/bootstrap/css/bootstrap.css" />" rel="stylesheet">
    </head>
    <body>
        
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          <li class="active"><a href="<c:url value='/profil' />">Profile</a></li>
          <li><a href="<c:url value='/contact' />">Contacts</a></li>
          <li><a href="<c:url value='/sendmessage' />">Messages</a></li>
          <li><a href="<c:url value='/offer' />">View offer</a></li>
          <li><a href="<c:url value='/logout' />">Logout</a></li>
        </ul>
    </div>
        
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="span6 offset5">
        
            <h1>My Profile</h1>
            
            </br>
            
            First name: ${userinfo.firstName}</br></br>
            Last name: ${userinfo.lastName}</br></br>  
            Address: ${userinfo.adress}</br></br>   
            Mail: ${userinfo.mailAdress}</br></br> 
            Phone number: ${userinfo.phoneNumber}</br></br>  
            Card number: ${userinfo.cardNumber}</br></br> 

            <a href="<c:url value='/edit_profil' />">Edit profile</a>
            
            </div>
        </div>
      </div>
    </body>
</html>
