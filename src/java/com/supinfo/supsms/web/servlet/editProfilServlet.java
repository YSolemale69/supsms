/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.web.servlet;

import com.supinfo.supsms.entity.User;
import com.supinfo.supsms.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author yannissolemale
 */
@WebServlet(name = "editProfilServlet", urlPatterns = {"/edit_profil"})
public class editProfilServlet extends HttpServlet {
    
    @EJB
    UserService userService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.getRequestDispatcher("/jsp/edit_profil.jsp").forward(req, resp);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User)req.getSession().getAttribute("user");

        user.setFirstName(req.getParameter("firstname"));
        user.setLastName(req.getParameter("lastname"));
        user.setPassword(req.getParameter("password"));
        user.setAdress(req.getParameter("adress"));
        user.setMailAdress(req.getParameter("mail"));
        user.setCardNumber(req.getParameter("card"));
        
        userService.updateUser(user);
                
        req.getSession().setAttribute("user", user);
        resp.sendRedirect(req.getContextPath() + "/profil");
    }
}
