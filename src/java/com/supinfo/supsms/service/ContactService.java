/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.service;

import com.supinfo.supsms.dao.ContactDao;
import com.supinfo.supsms.entity.Contact;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Pascal
 */
@Stateless
public class ContactService {

    @EJB
    private ContactDao contactDao;
    
    public Contact addContact(Contact contact){
        return contactDao.addContact(contact);
    }
    
    public Contact updateContact(Contact contact){
        return contactDao.updateContact(contact);
    }
    
    public Contact removeContact(Long contactId){
        return contactDao.removeContact(contactId);
    }
    
    public Contact findContactById(Long contactId){
        return contactDao.findContactById(contactId);
    }
    
    public List<Contact> findContactByUser(Long userId){
        return contactDao.findContactByUser(userId);
    }
    
    public Contact findContactByUserAndNumber(Long userId, String phoneNumber){
        return contactDao.findContactByUserAndNumber(userId, phoneNumber);
    }

    public Contact findContactByFirstNameAndLastName(String Firstname, String Lastname) {
        return contactDao.findContactByFirstNameAndLastName(Firstname, Lastname);
    }
}
