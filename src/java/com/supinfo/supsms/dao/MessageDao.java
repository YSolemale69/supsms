/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.dao;

import com.supinfo.supsms.entity.Message;
import com.supinfo.supsms.entity.User;
import java.util.List;

/**
 *
 * @author Pascal
 */
public interface MessageDao {
    
    Message addMessage(Message message);
    
    public List<Message> findMessageBySenderAndReceiver(String sender, String Receiver);
    
    public List<String> getAllConvers(User user);
    
    public List<Message> getAllMessages();
}
