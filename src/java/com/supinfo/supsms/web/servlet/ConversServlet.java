/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.web.servlet;

import com.supinfo.supsms.entity.Contact;
import com.supinfo.supsms.entity.Message;
import com.supinfo.supsms.entity.User;
import com.supinfo.supsms.service.ContactService;
import com.supinfo.supsms.service.MessageService;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pascal
 */
@WebServlet(name = "ConversServlet", urlPatterns = {"/convers"})
public class ConversServlet extends HttpServlet {

    @EJB
    MessageService messageService;
    
    @EJB
    ContactService contactService;
    
    @Resource(mappedName = "jms/myQueue")
    private Queue myQueue;
    @Inject
    @JMSConnectionFactory("jms/myQueueFactory")
    private JMSContext context;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String phone = req.getParameter("nb");
        req.getSession().setAttribute("nb", phone);
        User user = (User) req.getSession().getAttribute("user");
        Contact contact = contactService.findContactByUserAndNumber(user.getId(), phone);
        List<Message> convers = messageService.findMessageBySenderAndReceiver(user.getPhoneNumber(), phone);
        if(contact != null){
            req.setAttribute("contactInfo", contact);
        }
        req.setAttribute("user", user);
        req.setAttribute("messages", convers);
        req.getRequestDispatcher("/jsp/convers.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        
        User user = (User) req.getSession().getAttribute("user");
        String nb = (String) req.getSession().getAttribute("nb");
        Message msg = new Message();

        msg.setContent(req.getParameter("content"));
        msg.setReceiver(nb);
        msg.setSender(user.getPhoneNumber());
        msg.setDate(new java.util.Date().getTime());        
        String message=msg.getSender()+"\n\r"+msg.getContent()+"\n\r"+msg.getReceiver();
        /*SENDER\n\rMESSAGE\n\rRECIPIENT*/
        sendJMSMessageToMyQueue(message);
        messageService.addMessage(msg);
        req.removeAttribute("nb");
        resp.sendRedirect(getServletContext().getContextPath() + "/convers?nb=" + nb);
    }
    
    private void sendJMSMessageToMyQueue(String messageData) {
        context.createProducer().send(myQueue, messageData);
    }
}
