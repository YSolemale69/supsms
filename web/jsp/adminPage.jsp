<%-- 
    Document   : adminPage
    Created on : 2 déc. 2014, 21:33:49
    Author     : Opium
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SupSms - Administration</title>
        <link href="<c:url value="/jsp/bootstrap/css/bootstrap.css" />" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
        <div class="row-fluid">
            <div class="span10 offset1">
                
            <h1>ADM</h1>
            <table class="table">
                <tr>
                    <%--User ou customer ? --%>
                    <c:forEach items="${customer}" var="customer">
                        <c:url value="/admin" var="deleteCustomerUrl">
                            <c:param name="id" value="${customer.id}" />
                        </c:url>
                        <td>       
                            <c:if test="${not empty user}">
                                <a href="${deleteCustomerUrl}">Delete</a>
                            </c:if>
                        </td>
                    </c:forEach>
                </tr>
            </table>
            </div>
        </div>
        </div>
                    
    </body>
</html>
