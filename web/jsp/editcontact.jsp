<%-- 
    Document   : editcontact
    Created on : 18 déc. 2014, 16:19:30
    Author     : Pascal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Contact</title>
        <link href="<c:url value="/jsp/bootstrap/css/bootstrap.css" />" rel="stylesheet">
    </head>
    <body>
        
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          <li><a href="<c:url value='/profil' />">Profile</a></li>
          <li><a href="<c:url value='/contact' />">Contacts</a></li>
          <li><a href="<c:url value='/sendmessage' />">Messages</a></li>
          <li><a href="<c:url value='/offer' />">View offer</a></li>
          <li><a href="<c:url value='/logout' />">Logout</a></li>
        </ul>
    </div>
        
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="span6 offset5">
              
            <h1>Edit Contact</h1>
            <form method="POST">
                <div>
                    <label for="firstname">First Name</label>
                    <input type="text" name="firstname" value="<c:out value="${contactinfo.firstName}"/>"/>
                </div>
                <div>
                    <label for="lastname">Last Name</label>
                    <input type="text" name="lastname" value="<c:out value="${contactinfo.lastName}"/>"/>
                </div>
                <div>
                    <label for="phonenumber">Phone number</label>
                    <input type="text" name="phonenumber" value="<c:out value="${contactinfo.phoneNumber}"/>"/>
                </div>
                <div>
                    <label for="adress">Adress</label>
                    <input type="text" name="adress" value="<c:out value="${contactinfo.adress}"/>"/>
                </div>
                <div>
                    <label for="mail">Mail</label>
                    <input type="text" name="mail" value="<c:out value="${contactinfo.mailAdress}"/>"/>
                </div>

                <div>
                    <input class="btn btn-info" type="submit" value="Modify" />
                </div>
            </form>
                
          </div>
        </div>
    </div>
    </body>
</html>
