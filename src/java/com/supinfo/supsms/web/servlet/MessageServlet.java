/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.web.servlet;

import com.supinfo.supsms.entity.Contact;
import com.supinfo.supsms.entity.Message;
import com.supinfo.supsms.entity.User;
import com.supinfo.supsms.service.ContactService;
import com.supinfo.supsms.service.MessageService;
import java.io.IOException;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Rémy
 */
@WebServlet(name = "MessageServlet", urlPatterns = {"/sendmessage"})
public class MessageServlet extends HttpServlet {
    
    @EJB
    MessageService messageservice;
    
    @EJB
    ContactService contactService;
    
    @Resource(mappedName = "jms/myQueue")
    private Queue myQueue;
    @Inject
    @JMSConnectionFactory("jms/myQueueFactory")
    private JMSContext context;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {    
    User user = (User) req.getSession().getAttribute("user");
    req.setAttribute("sender", user.getPhoneNumber());
    List<Contact> contact = contactService.findContactByUser(user.getId());
    req.setAttribute("contacts",contact);
    req.getRequestDispatcher("/jsp/message.jsp").forward(req, resp);
    
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
        Message msg= new Message();     
        msg.setContent(req.getParameter("content"));
        msg.setReceiver(req.getParameter("receiver"));
        msg.setSender(req.getParameter("sender"));
        msg.setDate(new java.util.Date().getTime());        
        String message=msg.getSender()+"\n\r"+msg.getContent()+"\n\r"+msg.getReceiver();
        /*SENDER\n\rMESSAGE\n\rRECIPIENT*/
        sendJMSMessageToMyQueue(message);
        messageservice.addMessage(msg);
         resp.sendRedirect(req.getContextPath() + "/jsp/message");
        
    }

    private void sendJMSMessageToMyQueue(String messageData) {
        context.createProducer().send(myQueue, messageData);
    }
    
    }
   