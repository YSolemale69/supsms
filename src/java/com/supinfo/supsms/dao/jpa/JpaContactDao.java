/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.dao.jpa;

import com.supinfo.supsms.dao.ContactDao;
import com.supinfo.supsms.entity.Contact;
import com.supinfo.supsms.entity.Contact_;
import com.supinfo.supsms.entity.User;
import com.supinfo.supsms.entity.User_;
import java.util.Collection;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Pascal
 */
@Stateless
public class JpaContactDao implements ContactDao{
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Contact addContact(Contact contact){
        em.persist(contact);
        return contact;
    }

    @Override
    public Contact updateContact(Contact contact) {
        em.merge(contact);
        return contact;
    }
    
    @Override
    public Contact removeContact(Long contactId) {
        Contact contact = em.find(Contact.class, contactId);
        em.remove(contact);
        return contact;
    }

    @Override
    public Contact findContactById(Long contactId) {
        return em.find(Contact.class, contactId);
    }

    @Override
    public List<Contact> findContactByUser(Long userId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
        Root<Contact> contact = query.from(Contact.class);
        query.where(criteriaBuilder.equal(contact.get(Contact_.creator).get(User_.id), userId));
        
        return em.createQuery(query).getResultList();
    }
    
    @Override
    public Contact findContactByFirstNameAndLastName(String Firstname,String Lastname){
        
         CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
        Root<Contact> contact = query.from(Contact.class); 
        Predicate namePred = criteriaBuilder.equal(contact.get(Contact_.firstName), Firstname);
        Predicate nmPred = criteriaBuilder.equal(contact.get(Contact_.lastName), Lastname);
        query.where(namePred, nmPred);
        Contact result;
        try{
            result = em.createQuery(query).getSingleResult();
        }catch(NoResultException e){
            return null;
        }
        return result;
    }
    
    @Override
    public Contact findContactByUserAndNumber(Long userId, String phoneNumber){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Contact> query = criteriaBuilder.createQuery(Contact.class);
        Root<Contact> contact = query.from(Contact.class);
        Predicate userPred = criteriaBuilder.equal(contact.get(Contact_.creator).get(User_.id), userId);
        Predicate numberPred = criteriaBuilder.equal(contact.get(Contact_.phoneNumber), phoneNumber);
        query.where(userPred, numberPred);
        
        Contact result;
        try{
            result = em.createQuery(query).getSingleResult();
        }catch(NoResultException e){
            return null;
        }
        return result;
    }
}
