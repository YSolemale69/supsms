/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.web.servlet;

import com.supinfo.supsms.entity.User;
import com.supinfo.supsms.service.ContactService;
import com.supinfo.supsms.service.UserService;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Pascal
 */
@WebServlet(name = "RemoveContactServlet", urlPatterns = {"/removecontact"})
public class RemoveContactServlet extends HttpServlet {

    @EJB
    ContactService contactService;
    
    @EJB
    UserService userService;
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String idParam = req.getParameter("id");
        Long contactId = Long.valueOf(idParam);
        
        contactService.removeContact(contactId);
        User user = (User) req.getSession().getAttribute("user");
        req.getSession().setAttribute("user", userService.findUserById(user.getId()));
        
        resp.sendRedirect(getServletContext().getContextPath() + "/contact");
    }

}
