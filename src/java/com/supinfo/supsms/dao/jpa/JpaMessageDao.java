/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.dao.jpa;

import com.supinfo.supsms.dao.MessageDao;
import com.supinfo.supsms.entity.Message;
import com.supinfo.supsms.entity.Message_;
import com.supinfo.supsms.entity.User;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Pascal
 */

@Stateless
public class JpaMessageDao implements MessageDao{
    @PersistenceContext
    private EntityManager em;

    @Override
    public Message addMessage(Message message){
       em.persist(message);
        return message;
    }
    
    @Override
    public List<Message> findMessageBySenderAndReceiver(String sender, String receiver){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Message> query = criteriaBuilder.createQuery(Message.class);
        Root<Message> message = query.from(Message.class);
        
        Predicate sendToSend = criteriaBuilder.equal(message.get(Message_.sender), sender);
        Predicate sendToRec = criteriaBuilder.equal(message.get(Message_.sender), receiver);
        Predicate recToSend = criteriaBuilder.equal(message.get(Message_.receiver), sender);
        Predicate rectoRec = criteriaBuilder.equal(message.get(Message_.receiver), receiver);
        
        Predicate sendPred = criteriaBuilder.or(sendToSend, sendToRec);
        Predicate recPred = criteriaBuilder.or(rectoRec, recToSend);
        
        query.where(sendPred, recPred);

        return em.createQuery(query).getResultList();
    }

    @Override
    public List<String> getAllConvers(User user) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Message> query = criteriaBuilder.createQuery(Message.class);
        Root<Message> message = query.from(Message.class);
        
        Predicate isSend = criteriaBuilder.equal(message.get(Message_.sender), user.getPhoneNumber());
        Predicate isRec = criteriaBuilder.equal(message.get(Message_.receiver), user.getPhoneNumber());
        
        query.where(criteriaBuilder.or(isSend, isRec));
        List<Message> list = em.createQuery(query).getResultList();
        List<String> numbers = new ArrayList<String>();
        for (Message mess : list) {
            if(mess.getSender().equals(user.getPhoneNumber())){
                if(!numbers.contains(mess.getReceiver())){
                    numbers.add(mess.getReceiver());
                }
            }else{
                if(!(numbers.contains(mess.getSender()))){
                    numbers.add(mess.getSender());
                }
            }
        }
        return numbers;
    }
    
    @Override
    public List<Message> getAllMessages() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Message> query = criteriaBuilder.createQuery(Message.class);
        query.from(Message.class);
        return em.createQuery(query).getResultList();
    }
}
