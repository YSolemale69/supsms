/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.dao.jpa;

import com.supinfo.supsms.dao.AdminDao;
import com.supinfo.supsms.entity.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

/**
 *
 * @author Opium
 */
@Stateless
public class JpaAdminDao implements AdminDao {
    
    @PersistenceContext
    private EntityManager em;
    
    @Override
    public List<User> getAllUsers(){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        query.from(User.class);
        return em.createQuery(query).getResultList();
    }

    @Override
    public User deleteUser(Long userId){
        User user = em.find(User.class, userId);
        em.remove(user);
        return user;
    }
    
    @Override
    public User findUserById(Long userId){
        return em.find(User.class, userId);
    }

}
