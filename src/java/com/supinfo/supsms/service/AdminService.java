/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.service;

import com.supinfo.supsms.dao.AdminDao;
import com.supinfo.supsms.entity.User;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author Opium
 */
public class AdminService {

    @EJB
    private AdminDao adminDao;
    
    public List<User> getAllUsers() {
        return adminDao.getAllUsers();
    }
    
    public User deleteUser(Long userId) {
        return adminDao.deleteUser(userId);
    }

    public User findUserById(Long userId) {
        return adminDao.findUserById(userId);
    }
}
