<%-- 
    Document   : login
    Created on : 6 déc. 2014, 17:11:54
    Author     : Opium
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link href="<c:url value="/jsp/bootstrap/css/bootstrap.css" />" rel="stylesheet">
    </head>
    <body>
        
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          <li class="active"><a href="<c:url value='/login' />">Login</a></li>
          <li><a href="<c:url value='/register' />">Register</a></li>
        </ul>
    </div>
        
    <div class="container-fluid">
        <div class="row-fluid">
          <div class="span6 offset5">
            
              <h1>Login</h1>
              
              <form method="POST">
                 <div>
                     <label for="phonenumber">Phone number</label>
                     <input type="text" name="phonenumber" />
                 </div>
                 <div>
                     <label for="password">Password:</label>
                     <input type="password" name="password" />
                 </div>
                 <div>
                     <input class="btn btn-info" type="submit" value="Submit" />
                 </div>
             </form>
              
          </div>
        </div>
    </div>
    </body>
</html>
