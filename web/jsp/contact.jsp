<%-- 
    Document   : contact
    Created on : 14 déc. 2014, 21:50:12
    Author     : Pascal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contact management</title>
        <link href="<c:url value="/jsp/bootstrap/css/bootstrap.css" />" rel="stylesheet">
    </head>
    <body>
        
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
        <a class="brand" href="<c:url value='/'/>">Home</a>
        <ul class="nav">
          <li><a href="<c:url value='/profil' />">Profile</a></li>
          <li class="active"><a href="<c:url value='/contact' />">Contacts</a></li>
          <li><a href="<c:url value='/sendmessage' />">Messages</a></li>
          <li><a href="<c:url value='/offer' />">View offer</a></li>
          <li><a href="<c:url value='/logout' />">Logout</a></li>
        </ul>
    </div>
        
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span6 offset5">
            
            <h1>Contacts</h1>
            <form method="POST">
                <div>
                    <label for="firstname">First Name</label>
                    <input type="text" name="firstname" />
                </div>
                <div>
                    <label for="lastname">Last Name</label>
                    <input type="text" name="lastname" />
                </div>
                <div>
                    <label for="phonenumber">Phone number</label>
                    <input type="text" name="phonenumber" />
                </div>
                <div>
                    <label for="adress">Address</label>
                    <input type="text" name="adress" />
                </div>
                <div>
                    <label for="mail">Mail</label>
                    <input type="text" name="mail" />
                </div>

                <div>
                    <input class="btn btn-info" type="submit" value="Add" />
                </div>
            </form>
            
        </div>
      </div>
            
    <div class="row-fluid">
        <div class="span5 offset4">
            
            <table class="table">
                
            
                <c:forEach items="${contacts}" var="contact">
                <tr>
                    <td>${contact.firstName}</td>
                    <td>${contact.lastName}</td>
                    <td>${contact.phoneNumber}</td>
                        
                    <td>
                        <c:url value="/editcontact" var="editUrl">
                            <c:param name="id" value="${contact.id}" />
                        </c:url>
                        <a href="${editUrl}">Edit Contact</a>
                    </td>
                    
                    <td>
                        <c:url value="/removecontact" var="removeUrl">
                            <c:param name="id" value="${contact.id}" />
                        </c:url>
                        <a href="${removeUrl}">Delete Contact</a>
                    </td>
                </tr>
                </c:forEach>
            </table>
            
        </div>
      </div>
    </div>
    </body>
</html>
