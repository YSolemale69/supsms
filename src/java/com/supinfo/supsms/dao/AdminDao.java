/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.supsms.dao;

import com.supinfo.supsms.entity.User;
import java.util.List;

/**
 *
 * @author Opium
 */
public interface AdminDao {

    public List<User> getAllUsers();

    public User deleteUser(Long userId);

    public User findUserById(Long userId);
    
}
